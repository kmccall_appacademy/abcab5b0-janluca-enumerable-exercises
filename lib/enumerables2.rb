require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr << 0
  arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.each do |string|
    if substring(string, substring) == false
      return false
    end
  end
  true
end

def substring(long_string, substring)
  long_string.include?(substring)
end
# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.

def non_unique_letters(string)
  letters = {}
  string.chars.each do |char|
    if letters[char] == nil
      letters[char] = 1
    else
      letters[char] += 1
    end
  end
  multiples = letters.select { |key, value| value > 1 && key != " " }
  multiples.keys
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  sorted = string.split.sort_by(&:length)
  sorted[sorted.length - 2..sorted.length - 1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  alphabet = ('a'..'z').to_a
  doesnt_contain = []
  alphabet.each do |letter|
    if string.include?(letter) == false
      doesnt_contain << letter
    end
  end
  doesnt_contain
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  non_repeating = []
  (first_yr..last_yr).each do |year|
    if not_repeat_year?(year) == true
      non_repeating << year
    end
  end
  non_repeating
end

def not_repeat_year?(year)
  string_nums = year.to_s.split("")
  string_nums.each do |num|
    if string_nums.count(num) > 1
      return false
    end
  end
  true
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  wonder = []
  songs.each do |song|
    if no_repeats?(song, songs) && !wonder.include?(song)
      wonder << song
    end
  end
  wonder
end

def no_repeats?(song_name, songs)
  songs_array = songs
  songs_array.each_index do |index|
    if songs_array[index] == song_name && songs_array[index + 1] == song_name
      return false
    end
  end
  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  distance = 99
  shortest_distance = ""
  string.split.each do |word|
    clean_word = remove_punc(word)
    if c_distance(clean_word) < distance
      shortest_distance = clean_word
      distance = c_distance(clean_word)
    end
  end
  shortest_distance
end

def remove_punc(word)
  punctuation = ["!", "?", ".", ","]
  word.split("").select { |let| !punctuation.include?(let) }.join
end

def c_distance(word)
  ind = word.length - 1
  distance = 100
  while ind > 0
    character = word.chars[ind]
    if character == "c"
      return word.length - 1 - ind
    end
    ind -= 1
  end
  distance
end


# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

# def repeated_number_ranges(arr)
#   hash = Hash[arr.map.with_index.to_a]
#   indices = []
#   arr.each_index do |ind|
#     if arr[ind] != arr[ind - 1]
#       if ind != hash[arr[ind]]
#         indices << [ind, hash[arr[ind]]]
#       end
#     end
#   end
#   indices
# end

def repeated_number_ranges(arr)
  indices = []
  arr.each_with_index do |num, ind|
    last_index = find_last_index(num, ind, arr)
    if last_index != ind && num != arr[ind - 1]
      indices << [ind, last_index]
    end
  end
  indices
end

def find_last_index(num, current_ind, arr)
  ind = current_ind
  until arr[ind] != num
    ind += 1
  end
  ind - 1
end
